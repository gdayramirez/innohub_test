exports.reverseString = (string, same) => {
    return new Promise((resolve) => {
        let reverseString = ''
        for (var i = string.length - 1; i >= 0; --i) reverseString += string[i]
        resolve(same ? string + reverseString : reverseString)
    });
};


exports.BlackMarks = (value) => {
    return new Promise((resolve) => {
        resolve(parseInt(value) * parseInt(value))
    });
};


exports.callRecursive = (value) => {
    return new Promise((resolve) => {
        resolve(this.Recursive(value, 0))
    });
};

exports.Recursive = (value, aux) => {
    if (value > 0) {
        return this.Recursive(value - 1, value + aux)
    } else {
        return aux;
    }

};

exports.NoRecursive = (value) => {
    return new Promise((resolve) => {
        let suma = 0;
        for (var i = 1; i <= value; i++) suma += i;
        resolve(suma);
    });
};