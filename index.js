const  express = require('express');
const  bodyParser = require('body-parser');
const  app = express();
const  port = 3500;

const mainRoutes = require('./routes');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api/v1/',mainRoutes);

app.listen(port);
