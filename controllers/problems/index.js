const { reverseString, BlackMarks, NoRecursive, callRecursive } = require('../../utils');

exports.init = (req, res) => {
    const { id } = req.params;
    const { value } = req.body;
    switch (id) {
        case '1':
            return reverseString(value, false).then(success => {
                res.send({
                    message: 'success',
                    value: success
                });
            }).catch(error => {
                res.status(400).send({ code: 400, error });
            });


        case '2':
            return reverseString(value, true).then(success => {
                res.send({
                    message: 'success',
                    value: success
                });
            }).catch(error => {
                res.status(400).send({ code: 400, error });
            });

        case '3':
            return BlackMarks(value).then(success => {
                res.send({
                    message: 'success',
                    value: success
                });
            }).catch(error => {
                res.status(400).send({ code: 400, error });
            });

        case '4':
            return callRecursive(value).then(success => {
                res.send({
                    message: 'success',
                    value: success
                });
            }).catch(error => {
                res.status(400).send({ code: 400, error });
            });
        case '5':
            return NoRecursive(value).then(success => {
                res.send({
                    message: 'success',
                    value: success
                });
            }).catch(error => {
                res.status(400).send({ code: 400, error });
            });

        default: res.send(500);
    }
};