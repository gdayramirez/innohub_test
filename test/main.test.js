const { reverseString, BlackMarks, NoRecursive, callRecursive } = require('../utils')


describe('reverseString() function', function () {
    it('Should error out if no value & flag provided ', function () {
        reverseString(6, false);
    });
    it('Should error out if no value & flag provided ', function () {
        reverseString(11, true);
    });
});

describe('BlackMarks() function', function () {
    it('Should error out if no value provided ', function () {
        BlackMarks(20);
    });
});



describe('NoRecursive() function', function () {
    it('Should error out if no value provided ', function () {
        NoRecursive(6);
    });
});


describe('callRecursive() function', function () {
    it('Should error out if no value provided ', function () {
        callRecursive(6);
    });
});