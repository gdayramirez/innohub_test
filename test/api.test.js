const mainMiddleware = require('../controllers/problems')
let res = {
    send: (json) => console.log(json)
}


describe('API TEST', function () {
    it('Problem 1', function () {
        mainMiddleware.init({
            body: {
                value: "hola jose"
            },
            params: {
                id: '1'
            }
        }, res);
    });
    it('Problem 2', function () {
        mainMiddleware.init({
            body: {
                value: "jose hola"
            },
            params: {
                id: '2'
            }
        }, res);
    });
    it('Problem 3', function () {
        mainMiddleware.init({
            body: {
                value: 6
            },
            params: {
                id: '3'
            }
        }, res);
    });
    it('Problem 4', function () {
        mainMiddleware.init({
            body: {
                value: 6
            },
            params: {
                id: '4'
            }
        }, res);
    });
    it('Problem 5', function () {
        mainMiddleware.init({
            body: {
                value: 6
            },
            params: {
                id: '5'
            }
        }, res);
    });
});

