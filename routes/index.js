const api = require('express').Router();
const problems = require('../controllers/problems');

module.exports = (()=>{
    api.post('/problem/:id',
        problems.init
    );
    return api;
})();